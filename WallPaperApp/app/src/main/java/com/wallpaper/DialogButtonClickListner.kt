package com.wallpaper

/**
 * Created by Zaini on 11/28/2018.
 */
interface DialogButtonClickListner {
    fun onDownloadClicked(url: String)
    fun onSetWallpaperClicked(url: String)
}