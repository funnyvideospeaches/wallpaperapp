package com.wallpaper.rest

import com.wallpaper.models.WallpaperModel
import retrofit2.Call
import retrofit2.http.*


interface ApiService {
    @POST("api")
    fun getWallpapersList(@QueryMap parameter: Map<String, String>): Call<WallpaperModel>
}