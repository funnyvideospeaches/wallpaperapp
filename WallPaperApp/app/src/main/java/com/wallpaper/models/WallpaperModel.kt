package com.wallpaper.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class WallpaperModel {
    @SerializedName("totalHits")
    @Expose
    var totalHits: Int? = null

    @SerializedName("hits")
    @Expose
    var hits: ArrayList<Hit>? = null

    @SerializedName("total")
    @Expose
    var total: Int? = null

}