package com.wallpaper

import android.app.DownloadManager
import android.app.ProgressDialog
import android.app.WallpaperManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.wallpaper.Adapters.WallpaperListAdapter
import com.wallpaper.Helper.EndlessRecyclerOnScrollListener
import com.wallpaper.Helper.Utility
import com.wallpaper.databinding.ActivityMainBinding
import com.wallpaper.models.Hit
import com.wallpaper.models.WallpaperModel
import com.wallpaper.rest.ApiClient
import com.wallpaper.rest.ApiService
import retrofit2.Call
import retrofit2.Callback
import java.io.File
import java.io.IOException
import java.util.*


class MainActivity : AppCompatActivity(), DialogButtonClickListner {

    val TAG = this.javaClass.simpleName
    var wallpapersList: ArrayList<Hit> = ArrayList()
    var pageno = 1
    var wallpaperListAdapter: WallpaperListAdapter? = null

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initThings()

    }

    private fun initThings() {

        binding.apply {

            btnBackToolbar.setOnClickListener { onBackPressed() }

            setRecyclerAdapter()

            if (Utility.isNetworkAvailable(this@MainActivity)) {
                getWallpapersList(pageno)
            } else {
                toast("Internet Connection not Found")
            }

        }
    }

    private fun setRecyclerAdapter() {
        binding.apply {

            val layoutManager = LinearLayoutManager(this@MainActivity)
            rvWallpapers.layoutManager = layoutManager
            rvWallpapers.itemAnimator = DefaultItemAnimator()
            wallpaperListAdapter = WallpaperListAdapter(this@MainActivity, wallpapersList)
            wallpaperListAdapter?.setDialogClickListner(this@MainActivity)
            rvWallpapers.adapter = wallpaperListAdapter

            val endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener =
                object : EndlessRecyclerOnScrollListener() {
                    override fun onLoadMore() {
                        if (Utility.isNetworkAvailable(this@MainActivity)) {
                            pageno += 1
                            getWallpapersList(pageno)
                        } else {
                            toast("Internet Connection not Found")
                        }
                    }
                }

            rvWallpapers.addOnScrollListener(endlessRecyclerOnScrollListener)
        }
    }


    private fun getWallpapersList(pageno: Int) {
        binding.progressbarBottom.visibility = View.VISIBLE

        val apiService = ApiClient.client?.create(ApiService::class.java)

        val map = HashMap<String, String>()
        map.put("key", "8451685-46044f969a36345059e7ec604")
        map.put("per_page", "10")
        map.put("page", pageno.toString())

        val call = apiService?.getWallpapersList(map)
        call?.enqueue(object : Callback<WallpaperModel> {
            override fun onResponse(
                call: Call<WallpaperModel>,
                response: retrofit2.Response<WallpaperModel>
            ) {
                binding.progressbarBottom.visibility = View.GONE
                assert(response.body() != null)
                if (response.isSuccessful && response.body()!!.hits != null) {
                    wallpapersList.addAll(response.body()!!.hits!!)
                    wallpaperListAdapter?.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<WallpaperModel>, t: Throwable) {

                toast(t.localizedMessage)
                binding.progressbarBottom.visibility = View.GONE
            }
        })
    }


    override fun onDownloadClicked(url: String) {
        Log.d(TAG, "downloadFile: uRl = $url")
        val direct = File(Environment.getExternalStorageDirectory().toString() + "/Download")

        if (!direct.exists()) {
            direct.mkdirs()
        }
        val mgr = this.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadUri = Uri.parse(url)
        val request = DownloadManager.Request(downloadUri)

        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
            .setAllowedOverRoaming(false)
            .setTitle("Wallpaper")
            .setDescription("Wallpaper is downloading please wait..")
            .setDestinationInExternalPublicDir(
                "/Download",
                Calendar.getInstance().time.toString() + ".jpg"
            )

        mgr.enqueue(request)
    }

    override fun onSetWallpaperClicked(url: String) {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Wallpaper is setting please wait...")
        progressDialog.setCancelable(true)
        progressDialog.show()

        Picasso.get().load(url).placeholder(R.drawable.placeholdercover)
            .error(R.drawable.placeholdercover).into(object : com.squareup.picasso.Target {
                override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                    val wallpaperManager = WallpaperManager.getInstance(this@MainActivity)
                    try {
                        wallpaperManager.setBitmap(bitmap)
                    } catch (ex: IOException) {
                        ex.printStackTrace()
                        Log.e("TAG", "onBitmapLoaded: " + ex.message)
                        toast(ex.message + "")

                    }

                    progressDialog.dismiss()
                }

                override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {
                    progressDialog.dismiss()
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable) {

                    Log.e(TAG, "onPrepareLoad: ")
                }
            })
    }


    fun toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }


}
