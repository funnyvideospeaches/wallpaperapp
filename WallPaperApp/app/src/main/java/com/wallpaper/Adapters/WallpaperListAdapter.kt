package com.wallpaper.Adapters

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.jsibbold.zoomage.ZoomageView
import com.wallpaper.DialogButtonClickListner
import com.wallpaper.R
import com.wallpaper.models.Hit
import java.util.*

class WallpaperListAdapter(
    private val context: Context,
    private val wallpapersList: ArrayList<Hit>
) : RecyclerView.Adapter<WallpaperListAdapter.ViewHolder>() {
    private var dialogClickListner: DialogButtonClickListner? = null
    fun setDialogClickListner(dialogClickListner: DialogButtonClickListner?) {
        this.dialogClickListner = dialogClickListner
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_wallpaper, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        holder.tvViews.text = wallpapersList[position].views.toString()
        holder.tvLikes.text = wallpapersList[position].likes.toString()
        holder.tvComments.text = wallpapersList[position].comments.toString()
        holder.tvComments.isSelected = true
        holder.tvLikes.isSelected = true
        holder.tvViews.isSelected = true
        holder.ivWallpaper.setOnClickListener {
            ShowFullImage(
                context as Activity,
                wallpapersList[position].largeImageURL!!
            )
        }
        Glide.with(context).load(wallpapersList[position].largeImageURL)
            .apply(RequestOptions().override(200, 200))
            .thumbnail(0.5f).listener(object : RequestListener<Drawable?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Drawable?>,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progressBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any,
                    target: Target<Drawable?>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    holder.progressBar.visibility = View.GONE
                    return false
                }
            }).into(holder.ivWallpaper)
    }

    override fun getItemCount(): Int {
        return wallpapersList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun ShowFullImage(activity: Activity, image: String) {
        
        val dialogView = LayoutInflater.from(activity).inflate(R.layout.full_screen_image_dialog, null)
        val dialog = Dialog(context, R.style.full_screen_dialog)
        dialog.setContentView(dialogView)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        val btnDownload: Button
        val btnSetAsWallpaper: Button
        btnDownload = dialogView.findViewById(R.id.btnDownload)
        btnSetAsWallpaper = dialogView.findViewById(R.id.btnSetWallpaper)
        btnDownload.isSelected = true
        val progressBar = dialogView.findViewById<ProgressBar>(R.id.progressBar)
        progressBar.visibility = View.VISIBLE
        val zoomageView: ZoomageView = dialogView.findViewById(R.id.myZoomageView)

        btnDownload.setOnClickListener {
            if (checkPermission(context)) {
                dialogClickListner!!.onDownloadClicked(image)
                dialog.dismiss()
            }
        }

        btnSetAsWallpaper.setOnClickListener {
            dialogClickListner!!.onSetWallpaperClicked(image)
            dialog.dismiss()
        }

        Glide.with(context).load(image)
            .placeholder(R.drawable.placeholdercover).error(R.drawable.placeholdercover)
            .thumbnail(0.5f).listener(object : RequestListener<Drawable?> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Drawable?>,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any,
                    target: Target<Drawable?>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar.visibility = View.GONE
                    return false
                }
            })
            .into(zoomageView)

        dialog.show()
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var ivWallpaper: ImageView
        var tvLikes: TextView
        var tvComments: TextView
        var tvViews: TextView
        var progressBar: ProgressBar

        init {
            ivWallpaper = itemView.findViewById(R.id.ivWallpaper)
            tvComments = itemView.findViewById(R.id.tvComments)
            tvLikes = itemView.findViewById(R.id.tvLikes)
            tvViews = itemView.findViewById(R.id.tvViews)
            progressBar = itemView.findViewById(R.id.pbloading)
        }
    }

    companion object {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        fun checkPermission(context: Context?): Boolean {
            val currentAPIVersion = Build.VERSION.SDK_INT
            return if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        context!!,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            (context as Activity?)!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        )
                    ) {
                        val alertBuilder =
                            AlertDialog.Builder(context)
                        alertBuilder.setCancelable(true)
                        alertBuilder.setTitle("Permission necessary")
                        alertBuilder.setMessage("External storage permission is necessary")
                        alertBuilder.setPositiveButton(
                            android.R.string.yes
                        ) { dialog, which ->
                            ActivityCompat.requestPermissions(
                                (context as Activity?)!!,
                                arrayOf(
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                                ),
                                123
                            )
                        }
                        val alert = alertBuilder.create()
                        alert.show()
                    } else {
                        ActivityCompat.requestPermissions(
                            (context as Activity?)!!,
                            arrayOf(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ),
                            123
                        )
                    }
                    false
                } else {
                    true
                }
            } else {
                true
            }
        }
    }

}